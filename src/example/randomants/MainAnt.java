package example.randomants;

import mas.core.Agent;
import mas.core.Cyclable;
import mas.environment.TwoDCContinuousGrid;
import mas.implementation.schedulers.variations.TwoDCycling;
import mas.ui.MainWindow;
import mas.ui.SchedulerToolbar;

public class MainAnt {

    public static void main(String[] args) {

        class MyTwoDCycling extends TwoDCycling{

            AntHill antHill;

            public MyTwoDCycling(AntHill _anthill, Cyclable... _cyclables){
                super(_cyclables);
                antHill = _anthill;
            }

            @Override
            protected void onCycleEnds() {
                antHill.getAntsCountLabel().setText("Ants count " + Ant.getNumberOfAnts());
            }
        }

        int widht = 800;
        int height = 600;


        AntHill hill = new AntHill(widht, height);

        TwoDCContinuousGrid env = new TwoDCContinuousGrid(widht, height);

        int nAgents = 50;

        Agent[] ants = new Ant[nAgents];

        for(int i = 0; i<nAgents ; i++){
            ants[i] = new Ant(i+1,0,0,env);
        }

        MyTwoDCycling scheduler = new MyTwoDCycling(hill, ants);

        MainWindow.instance();
        MainWindow.addToolbar(new SchedulerToolbar("Amas", scheduler));
    }
}
