package example.randomants;

import mas.core.Agent;
import mas.environment.TwoDCContinuousGrid;
import mas.ui.VUI;
import mas.ui.drawables.DrawableImage;

import java.util.Random;

/**
 * An ant.
 */
public class Ant extends Agent {

    /**
     * Number of ants.
     */
    private static int numberOfAnts = 0;

    /**
     * id of the ant.
     */
    public int id;
    /**
     * X coordinate of the ant in the world.
     */
    public double dx;
    /**
     * Y coordinate of the ant in the world.
     */
    public double dy;

    public boolean mustDie = false;
    /**
     * Angle in radians.
     */
    private double angle = Math.random() * Math.PI * 2;
    private DrawableImage image;

    TwoDCContinuousGrid environment;

    /**
     * Constructor of the ant.
     *
     * @param _id
     *            Id of the ant
     * @param startX
     *            Initial X coordinate
     * @param startY
     *            Initial Y coordinate
     * @param _environment
     *            environment of the ant
     */
    public Ant(int _id, double startX, double startY, TwoDCContinuousGrid _environment) {
        numberOfAnts++;
        id = _id;
        dx = startX;
        dy = startY;
        environment = _environment;
        image = VUI.get().createImage(dx, dy, "src/example/randomants/ressources/ant.png");
    }

    /**
     * Move in a random direction.
     */
    @Override
    public void decide() {
        double random = new Random().nextGaussian();

        angle += random * 0.1;
        dx += Math.cos(angle);
        dy += Math.sin(angle);
        while (dx >= environment.getWidth() / 2)
            dx -= environment.getWidth();
        while (dy >= environment.getHeight() / 2)
            dy -= environment.getHeight();
        while (dx < -environment.getWidth() / 2)
            dx += environment.getWidth();
        while (dy < -environment.getHeight() / 2)
            dy += environment.getHeight();
    }

    @Override
    public boolean terminate() {
        return mustDie;
    }

    @Override
    public void act() {
        image.move(dx,dy);
        image.setAngle(angle);

        if (new Random().nextDouble() < 0.001) {
            scheduler.addCyclable(new Ant(id * 10,dx, dy, environment));
        }

        if (new Random().nextDouble() < 0.001) {
            image.setFilename("src/example/randomants/ressources/ant_dead.png");
            numberOfAnts--;
            mustDie = true;
        }
        //System.out.println("Ant n°" + id + " / x = " + dx + " / y = " + dy);
    }

    public static int getNumberOfAnts() {
        return numberOfAnts;
    }
}
