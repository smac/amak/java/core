package example.randomants;

import mas.ui.VUI;
import mas.ui.drawables.DrawableRectangle;
import mas.ui.drawables.DrawableString;

import java.awt.*;

/**
 * This class creates the VUI for the experience.
 */
public class AntHill {

    /**
     * Displayable that shows the current number of ants.
     */
    public DrawableString antsCountLabel;

    /**
     * Constructor of the AntHill.
     *
     * @param _width
     *          width of the world
     * @param _height
     *          height of the world
     */
    public AntHill(int _width, int _height){

        DrawableRectangle d = VUI.get().createRectangle(0, 0, _width, _height);
        d.setStrokeOnly();

        VUI.get().createRectangle(90, 20, 180, 40).setColor(new Color(0.9f, 0.9f, 0.9f, 0.8f)).setFixed().setLayer(5);

        VUI.get().createImage(20, 20, "src/example/randomants/ressources/ant.png").setFixed().setLayer(10);
        antsCountLabel = (DrawableString) VUI.get().createString(45, 25, "Ants count").setFixed().setLayer(10);
    }

    /**
     * Getter of the antsCountLabel
     *
     * @return a DrawableString with the current number of ants
     */
    public DrawableString getAntsCountLabel() {
        return antsCountLabel;
    }

    /**
     * Setter of the antsCountLabel
     *
     * @param _antsCountLabel
     *        The DrawableString with the current number of Ants
     */
    public void setAntsCountLabel(DrawableString _antsCountLabel) {
        antsCountLabel = _antsCountLabel;
    }
}
