package example.philosophes;

import mas.core.Cyclable;
import mas.core.Scheduler;

/**
 * This class is useful to see how the scheduler adds new cyclables to the system dynamically.
 */
public class Waste implements Cyclable {

    private int id;

    Scheduler scheduler = null;

    public Waste(int _id){
        id = _id;

    }
        @Override
    public void cycle() {
        //System.out.println("I'm the waste n°" + id);
    }

    @Override
    public boolean terminate() {
        return true;
    }

    @Override
    public void setScheduler(Scheduler _scheduler) {
        scheduler = _scheduler;
    }

    @Override
    public String toString() {
        return "Waste " + id;
    }
}
