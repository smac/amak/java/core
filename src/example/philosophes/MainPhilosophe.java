package example.philosophes;

import mas.core.Cyclable;
import mas.implementation.schedulers.FairCycling;

import java.util.ArrayList;
import java.util.List;

public class MainPhilosophe {

    public static void main(String[] args) {

        int nbCycles = 100;
        int nAgents = 100;

        /**
         * An extension of the scheduler to make some performance tests
         */
        class MyFairCycling extends FairCycling<Cyclable> {

            long startTimeCycle = 0;

            List<Long> cycleTime = new ArrayList<>();

            public MyFairCycling(Cyclable... _cyclables){
                super(_cyclables);
            }

            @Override
            public boolean stopCondition() {
                return nbOfCycles == nbCycles;
            }

            @Override
            protected void onCycleStarts() {
                startTimeCycle = System.nanoTime();
            }

            @Override
            protected void onCycleEnds() {
                cycleTime.add(System.nanoTime() - startTimeCycle);
            }

            public double getCycleTime(){
                double moyenne = cycleTime.stream().mapToLong(value -> value).average().orElse(0.0);
                return moyenne;
            }
        }

        Philosopher[] philosophers = new Philosopher[nAgents];
        Fork[] forks = new Fork[nAgents];

        for (int i = 0; i<nAgents ; i++){
            forks[i] = new Fork();
        }

        for(int i = 0; i<nAgents ; i++){
            philosophers[i] = new Philosopher(i, forks[(((i-1) % nAgents) + nAgents) % nAgents], forks[i], null, null);
        }

        for (int i = 0; i<nAgents ; i++){
            philosophers[i].setLeftPhilosopher(philosophers[(((i-1) % nAgents) + nAgents) % nAgents]);
            philosophers[i].setRightPhilosopher(philosophers[(i+1) % nAgents]);
        }

        //Sequential execution
        sequentialPerformanceTest(100, philosophers);


        //Amak execution
        MyFairCycling scheduler = new MyFairCycling(philosophers);
        scheduler.setSleep(0);

        final long startTime = System.nanoTime();

        scheduler.start();

        scheduler.waitUntilFinish();

        final long endTime = System.nanoTime();
        System.out.println("AMAk : Total execution time: " + (endTime / 1000 - startTime / 1000) + " microseconds");
        System.out.println("\tCycle moyen : " + scheduler.getCycleTime() / 1000 + " microseconds");
    }

    public static void sequentialPerformanceTest(int nbCycles, Cyclable... cyclables){
        final long startTimeSequential = System.nanoTime();

        int nbCycle = 0;
        long startTimeCycleSequential = 0;
        List<Long> cycleTimeSequential = new ArrayList<>();

        while(nbCycle < nbCycles){
            startTimeCycleSequential = System.nanoTime();

            for(Cyclable cyclable : cyclables){
                cyclable.cycle();
            }

            cycleTimeSequential.add(System.nanoTime() - startTimeCycleSequential);

            nbCycle++;
        }

        final long endTimeSequential = System.nanoTime();

        System.out.println("Iterative : Total execution time: " + (endTimeSequential / 1000 - startTimeSequential / 1000) + " microseconds");
        System.out.println("\tCycle : " + cycleTimeSequential.stream().mapToLong(value -> value).average().orElse(0.0) /1000 + " microseconds");
    }
}
