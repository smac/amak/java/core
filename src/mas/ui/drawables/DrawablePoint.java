package mas.ui.drawables;


import mas.ui.VUI;

public class DrawablePoint extends DrawableRectangle {

	public DrawablePoint(VUI vui, double dx, double dy) {
		super(vui, dx, dy, 10, 10);
	}

}
