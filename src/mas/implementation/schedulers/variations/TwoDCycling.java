package mas.implementation.schedulers.variations;

import mas.core.Cyclable;
import mas.implementation.schedulers.FairCycling;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

/**
 * Extension of the {@link FairCycling} scheduler made to be used with a GUI.
 */
public class TwoDCycling extends FairCycling<Cyclable> {

    /**
     * The state of the scheduler {@link State}
     */
    private State state = State.PENDING_START;

    /**
     * Method that is called when the scheduler stops
     */
    private Consumer<TwoDCycling> onStop;


    /**
     * The methods called when the speed is changed. Useful to change the value of
     * the GUI slider of {@link mas.ui.SchedulerToolbar}
     */
    private final List<Consumer<TwoDCycling>> onChange = new ArrayList<>();

    protected enum State {
        /**
         * The scheduler is waiting to start
         */
        PENDING_START,
        /**
         * The scheduler is running
         */
        RUNNING,
        /**
         * The scheduler is paused
         */
        IDLE

    }

    /**
     * Constructor which set the initial cyclables.
     *
     * @param _cyclables
     *          The corresponding cyclables
     */
    public TwoDCycling(Cyclable... _cyclables){
        super(_cyclables);
    }

    /**
     * Set the method that must be executed when the system is stopped
     *
     * @param _onStop
     *            Consumer method
     */
    public final void setOnStop(Consumer<TwoDCycling> _onStop) {
        this.onStop = _onStop;
    }

    /**
     * Add a method that must be executed when the scheduler speed is changed
     *
     * @param _onChange
     *            Consumer method
     */
    public final void addOnChange(Consumer<TwoDCycling> _onChange) {
        synchronized (onChange) {
            this.onChange.add(_onChange);
        }
    }

    /**
     * Method that performs only one system cycle.
     */
    public void doOneCycle() {
        executor.execute(() -> {
            step();
            if(stopCondition()){
                this.stop();
            }

            if (!mustStop) {
                this.pause();
            }
        });
    }

    /**
     * Method that launches or resumes the scheduler with a certain sleep time.
     *
     * @param _sleep
     *          The sleep of the scheduler
     */
    public void startWithSleep(int _sleep){
        setSleep(_sleep);


        if (state == State.PENDING_START) {
            start();
        } else {
            resume();
        }
    }

    /**
     * This method says whether the scheduler is running
     *
     * @return whether the scheduler is running
     */
    public boolean isRunning() {
        return state == State.RUNNING;
    }

    @Override
    public void start() {
        state = State.RUNNING;
        super.start();
    }

    @Override
    public void pause() {
        state = State.IDLE;
        if(pauseLatch == null || pauseLatch.getCount() == 0){
            pauseLatch = new CountDownLatch(1);
        }
        mustPause = true;
    }

    @Override
    public void resume() {
        super.resume();
        state = State.RUNNING;
    }
}
