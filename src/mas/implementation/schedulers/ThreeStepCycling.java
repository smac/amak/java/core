package mas.implementation.schedulers;

import mas.core.ThreeStepCyclable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

/**
 * The ThreeStepCycling scheduler schedules tasks using a {@link Executors#newCachedThreadPool()}.
 * It works like a {@link FairCycling} scheduler but it schedules {@link ThreeStepCyclable}
 * In every cycle every cyclable does the perception phase then the decision phase and then the action phase
 */
public class ThreeStepCycling extends FairCycling<ThreeStepCyclable> {

    /**
     * Object that synchronize each cyclable every system's cycle for the perception phase.
     */
    protected CountDownLatch perceptionLatch;

    /**
     * Object that synchronize each cyclable every system's cycle for the decision phase.
     */
    protected CountDownLatch decicionLatch;

    /**
     * Constructor which set the initial cyclables.
     *
     * @param _threeStepCyclables
     *          The corresponding cyclables
     */
    public ThreeStepCycling(ThreeStepCyclable... _threeStepCyclables){
        super(_threeStepCyclables);
    }

    @Override
    protected void step() {
        onCycleStarts();

        //treatPendingCyclables();

        cycleLatch = new CountDownLatch(cyclables.size());
        perceptionLatch = new CountDownLatch(cyclables.size());
        decicionLatch = new CountDownLatch(cyclables.size());

        for(ThreeStepCyclable threeStepCyclable : cyclables){

            executor.execute(() -> {
                threeStepCyclable.perceive();

                waitPerception();

                threeStepCyclable.decide();

                waitDecision();

                threeStepCyclable.act();

                if(threeStepCyclable.terminate()){
                    //pendingToAddCyclables.add(threeStepCyclable);
                }
                cycleLatch.countDown();
            });
        }

        doSleep();

        waitAction();

        onCycleEnds();

        cyclables.clear();

        nbOfCycles++;
    }

    /**
     * This method synchronize every cyclable in the perception phase.
     */
    protected  void waitPerception(){
        perceptionLatch.countDown();

        try {
            perceptionLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This method synchronize every cyclable in the decision phase.
     */
    protected void waitDecision(){
        decicionLatch.countDown();

        try {
            decicionLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * This method synchronize every cyclable in the action phase.
     */
    protected void waitAction(){
        try {
            cycleLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
