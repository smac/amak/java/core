package mas.implementation.schedulers;

import mas.core.Cyclable;
import mas.core.Scheduler;
import mas.core.Sleepable;

import java.util.*;
import java.util.concurrent.*;

/**
 * The FairCycling scheduler schedules tasks using a {@link Executors#newCachedThreadPool()}.
 * Every cyclable executes its cycle once every system's cycle.
 *
 * @param <T>
 *          Extended class of {@link Cyclable}
 *
 * @author David Antunes
 */
public class FairCycling<T extends Cyclable> implements Scheduler<T>, Sleepable {

    /**
     * The cyclable objects handled by the scheduler.
     */
    protected Set<T> cyclables = new LinkedHashSet<>();

    /**
     * The cyclables that must be added in the next cycle.
     */
    protected Queue<T> pendingToAddCyclables = new ConcurrentLinkedQueue<>();

    /**
     * Time between two cycles. Default time in {@link Sleepable#DEFAULT_SLEEP}.
     */
    protected int sleep = DEFAULT_SLEEP;

    /**
     * Number of system cycles.
     */
    protected int nbOfCycles = 0;

    /**
     * Condition to know if the scheduler must be stopped.
     */
    protected boolean mustStop = false;

    /**
     * Condition to know if the scheduler must be paused.
     */
    protected boolean mustPause = false;

    /**
     * The executor of the tasks.
     */
    protected ExecutorService executor = Executors.newCachedThreadPool();

    /**
     * Object used to pause the scheduler.
     */
    protected CountDownLatch pauseLatch;

    /**
     * Object that synchronize each cyclable every system's cycle.
     */
    protected CountDownLatch cycleLatch;

    /**
     * Constructor which set the initial cyclables.
     *
     * @param _cyclables
     *          The corresponding cyclables
     */
    @SafeVarargs
    public FairCycling(T... _cyclables) {

        for (T cyclable : _cyclables) {
            addCyclable(cyclable);
        }
    }

    @Override
    public void start() {
        executor.execute(this::doCycle);
    }

    @Override
    public void stop() {
        mustStop = true;
        executor.shutdown();
    }

    @Override
    public void pause() {
        pauseLatch = new CountDownLatch(1);
        mustPause = true;
    }

    @Override
    public void resume() {
        if(pauseLatch != null){
            pauseLatch.countDown();
        }
    }

    @Override
    public void addCyclable(T cyclable) {
        cyclable.setScheduler(this);
        pendingToAddCyclables.add(cyclable);
    }

    @Override
    public boolean stopCondition(){
        return false;
    }

    @Override
    public boolean isFinished() {
        return executor.isTerminated();
    }

    @Override
    public void waitUntilFinish() {
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getSleep() {
        return sleep;
    }

    @Override
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    @Override
    public void doSleep(){
        if (getSleep() != 0) {
            try {
                Thread.sleep(sleep);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is called at the end of every system's cycle.
     */
    protected void onCycleEnds() {

    }

    /**
     * This method is called at the start of every system's cycle.
     */
    protected void onCycleStarts(){

    }

    /**
     * Executes a system's cycle
     */
    protected void step() {
        onCycleStarts();

        treatPendingCyclables();

        cycleLatch = new CountDownLatch(cyclables.size());

        for (T cyclable : cyclables) {

            executor.execute(() -> {
                cyclable.cycle();
                if(!cyclable.terminate()){
                    pendingToAddCyclables.add(cyclable);
                }
                cycleLatch.countDown();
            });
        }

        doSleep();

        try {
            cycleLatch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        onCycleEnds();

        cyclables.clear();

        nbOfCycles++;
    }

    /**
     * Executes a {@link #step()} and set up the next step if needed.
     */
    protected void doCycle() {
        step();
        if(stopCondition()){
            this.stop();
        }

        if (!mustStop) {
            if (mustPause) {
                try {
                    pauseLatch.await();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            executor.execute(this::doCycle);
        }
    }

    /**
     * Add the cyclables that are going to be scheduled on the current cycle.
     */
    protected void treatPendingCyclables() {
        cyclables.addAll(pendingToAddCyclables);
        pendingToAddCyclables.clear();
    }

    /**
     * Getter for the number of cycles.
     *
     * @return the number of cycles performed by the system
     */
    public int getNbOfCycles() {
        return nbOfCycles;
    }
}
