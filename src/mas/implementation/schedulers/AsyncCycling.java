package mas.implementation.schedulers;

import mas.core.Cyclable;
import mas.core.Scheduler;
import mas.core.Sleepable;

import java.util.LinkedHashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.*;

/**
 * The AsyncCycling scheduler schedules tasks asynchronously using a {@link PausableThreadPoolExecutor}.
 *
 * @author David Antunes
 */
public class AsyncCycling<T extends Cyclable> implements Scheduler<T>, Sleepable {

    /**
     * The cyclable objects handled by the scheduler.
     */
    protected final Queue<T> cyclables = new ConcurrentLinkedDeque<>();

    /**
     * Time between two cycles. Default time in {@link Sleepable#DEFAULT_SLEEP}.
     */
    private int sleep = DEFAULT_SLEEP;

    /**
     * Condition to know if the scheduler must be stopped.
     */
    protected boolean mustStop = false;

    /**
     * The executor of the tasks.
     */
    protected PausableThreadPoolExecutor executor = new PausableThreadPoolExecutor();

    /**
     * Constructor which set the initial cyclables.
     *
     * @param _cyclables
     *          The corresponding cyclables
     */
    @SafeVarargs
    public AsyncCycling(T... _cyclables){

        for(T cyclable : _cyclables){
            cyclables.add(cyclable);
            cyclable.setScheduler(this);
        }
    }

    @Override
    public void start() {
        for (T cyclable : cyclables){
            executor.execute(() -> manageCyclable(cyclable));
        }
    }

    @Override
    public void stop() {
        mustStop = true;
        executor.shutdown();
    }

    @Override
    public void pause() {
        executor.pause();
    }

    @Override
    public void resume() {
        executor.resume();
    }

    @Override
    public void addCyclable(T cyclable) {
        cyclables.add(cyclable);
        cyclable.setScheduler(this);

        if(!mustStop){
            executor.execute(() -> manageCyclable(cyclable));
        }
    }

    @Override
    public boolean stopCondition() {
        return false;
    }

    @Override
    public boolean isFinished() {
        return executor.isTerminated();
    }

    @Override
    public void waitUntilFinish() {
        try {
            executor.awaitTermination(Long.MAX_VALUE,TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getSleep() {
        return sleep;
    }

    @Override
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    @Override
    public void doSleep(){
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Executes a cycle and set up the next cycle if needed.
     *
     * @param cyclable
     *          The cyclable
     */
    protected void manageCyclable(T cyclable){
        cyclable.cycle();

        doSleep();

        if(!cyclable.terminate() && !mustStop){
            executor.execute(() -> manageCyclable(cyclable));
        } else {
            cyclables.remove(cyclable);
        }
    }
}
