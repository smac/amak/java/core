package mas.implementation.schedulers;

import mas.core.Cyclable;
import mas.core.Scheduler;
import mas.core.Sleepable;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

/**
 * The MonoThreadedCycling scheduler schedules tasks using a {@link Thread}. The execution is sequential.
 * This scheduler is very efficient with low complexity cyclables.
 * Every cyclable executes its cycle once every system's cycle.
 *
 * @param <T>
 *          Extended class of {@link mas.core.Cyclable}
 *
 * @author David Antunes
 */
public class MonoThreadedCycling<T extends Cyclable> implements Scheduler<T>, Sleepable {

    /**
     * The cyclable objects handled by the scheduler.
     */
    protected List<T> cyclables = new ArrayList<>();

    /**
     * The cyclables that must be added in the next cycle.
     */
    protected Queue<T> pendingToAddCyclables = new ConcurrentLinkedQueue<>();

    /**
     * Time between two cycles. Default time in {@link Sleepable#DEFAULT_SLEEP}.
     */
    protected int sleep = DEFAULT_SLEEP;

    /**
     * Number of system cycles.
     */
    protected int nbOfCycles = 0;

    /**
     * Condition to know if the scheduler must be stopped.
     */
    protected boolean mustStop = false;

    /**
     * Condition to know if the scheduler must be paused.
     */
    protected boolean mustPause = false;

    /**
     * Object used to pause the scheduler.
     */
    protected CountDownLatch pauseLatch;

    /**
     * The thread which executes the cycle.
     */
    Thread executionThread = new Thread(this::doCycle);

    /**
     * Constructor which set the initial cyclables.
     *
     * @param cyclables
     *          The corresponding cyclables
     */
    @SafeVarargs
    public MonoThreadedCycling(T... cyclables){
        for (T cyclable : cyclables) {
            addCyclable(cyclable);
        }
    }

    @Override
    public void start() {
        executionThread.start();
    }

    @Override
    public void stop() {
        mustStop = true;
    }

    @Override
    public void pause() {
        if(pauseLatch == null || pauseLatch.getCount() == 0){
            pauseLatch = new CountDownLatch(1);
        }
        mustPause = true;
    }

    @Override
    public void resume() {
        if(pauseLatch != null){
            pauseLatch.countDown();
        }
    }

    @Override
    public void addCyclable(T cyclable) {
        cyclable.setScheduler(this);
        pendingToAddCyclables.add(cyclable);
    }

    @Override
    public boolean stopCondition() {
        return false;
    }

    @Override
    public boolean isFinished() {
        return !executionThread.isAlive();
    }

    @Override
    public void waitUntilFinish() {
        try {
            executionThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int getSleep() {
        return sleep;
    }

    @Override
    public void setSleep(int sleep) {
        this.sleep = sleep;
    }

    @Override
    public void doSleep() {
        if (getSleep() != 0) {
            try {
                Thread.sleep(sleep);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Executes {@link #step()} until the scheduler must stop.
     */
    protected void doCycle(){
        while(!mustStop){
            step();
            if(stopCondition()){
                this.stop();
            }

            if(mustPause){
                try{
                    pauseLatch.await();
                } catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * Executes a system's cycle.
     */
    protected void step(){
        onCycleStarts();

        treatPendingCyclables();

        for(T cyclable : cyclables){
            cyclable.cycle();
            if(!cyclable.terminate()){
                pendingToAddCyclables.add(cyclable);
            }
        }

        doSleep();

        onCycleEnds();

        cyclables.clear();

        nbOfCycles++;
    }

    /**
     * Add the cyclables that are going to be scheduled on the current cycle.
     */
    protected void treatPendingCyclables() {
        cyclables.addAll(pendingToAddCyclables);
        Collections.shuffle(cyclables);

        pendingToAddCyclables.clear();
    }

    /**
     * This method is called at the end of every system's cycle.
     */
    protected void onCycleEnds() {

    }

    /**
     * This method is called at the start of every system's cycle.
     */
    protected void onCycleStarts(){

    }

    /**
     * Getter for the number of cycles.
     *
     * @return the number of cycles performed by the system
     */
    public int getNbOfCycles() {
        return nbOfCycles;
    }
}
