package mas.core;

/**
 * A sleepable object. Useful for schedulers which can have sleep time between cycles.
 */
public interface Sleepable {

    /**
     * Default sleep time
     */
    int DEFAULT_SLEEP = 0;

    /**
     * Getter for the sleep time.
     *
     * @return the current time elapsed between each cycle
     */
    int getSleep();

    /**
     * Setter for the sleep time.
     *
     * @param sleep
     *          The time between each cycle
     */
    void setSleep(int sleep);

    /**
     * Performs the waiting time between two cycles.
     */
    void doSleep();
}
