package mas.core;

/**
 * A cyclable objet. This objet must be scheduled by a {@link Scheduler}.
 */
public interface Cyclable {

    /**
     * This method represents the agent's cycle.
     */
    void cycle();

    /**
     * Condition to stop the execution of an agent at the end of the next cycle.
     *
     * @return true if the cyclable should finish its execution, false otherwise.
     */
    boolean terminate();

    /**
     * Settter for the scheduler
     *
     * @param _scheduler
     *         The scheduler in which the cyclable is going to be scheduled.
     */
    void setScheduler(Scheduler _scheduler);
}
