package mas.core;

/**
 * An implementation of a threeStepCyclable agent.
 */
public class Agent implements ThreeStepCyclable{

    /**
     * The scheduler of the agent.
     */
    protected Scheduler scheduler;

    @Override
    public void perceive() {

    }

    @Override
    public void decide() {

    }

    @Override
    public void act() {

    }

    @Override
    public boolean terminate() {
        return false;
    }

    @Override
    public void setScheduler(Scheduler _scheduler) {
        scheduler = _scheduler;
    }
}
