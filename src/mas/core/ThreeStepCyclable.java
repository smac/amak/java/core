package mas.core;

/**
 * The ThreeStepCyclable interface should be implemented by any agent whose cycle is defined in three steeps (perceive, decide and act).
 */
public interface ThreeStepCyclable extends Cyclable {

    @Override
    default void cycle(){
        perceive();
        decide();
        act();
    }

    /**
     * This method represents the perception phase of the agent.
     */
    void perceive();

    /**
     * This method represents the decision phase of the agent.
     */
    void decide();

    /**
     * This method represents the phase in which the agent performs its action.
     */
    void act();
}
