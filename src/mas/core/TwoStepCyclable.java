package mas.core;

/**
 * The TwoStepCyclable interface should be implemented by any agent whose cycle is defined in two steeps (perceive and decideAndAct).
 */
public interface TwoStepCyclable extends Cyclable {

    @Override
    default void cycle(){
        perceive();
        decideAndAct();
    }

    /**
     * This method represents the perception phase of the agent.
     */
    void perceive();

    /**
     * This method represents the decideAndAct phase of the agent.
     */
    void decideAndAct();
}
