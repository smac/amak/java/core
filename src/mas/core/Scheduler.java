package mas.core;

/**
 * A schedulable object. Made to code schedulers.
 */
public interface Scheduler<T extends Cyclable> {

    /**
     * Launch the scheduler if it is not running.
     */
    void start();

    /**
     * Stops the scheduler if it is running.
     */
    void stop();

    /**
     * Pause the scheduler at the end of the current cycle.
     */
    void pause();

    /**
     * Resumes the scheduler in his current state.
     */
    void resume();

    /**
     * Add a Cyclable object to the scheduler and starts their cycle as soon as possible.
     *
     * @param cyclable
     *      The cyclable to add
     */
    void addCyclable(T cyclable);


    /**
     * This method allows the scheduler to stop on certain conditions.
     *
     * @return whether the scheduler must stop.
     */
    boolean stopCondition();

    /**
     * Shows if the scheduler as been stopped.
     *
     * @return if the scheduler as been stopped.
     */
    boolean isFinished();

    /**
     * Pause the execution until the executor has finished.
     *
     * This function needs to be called after a call to {@link Scheduler#stop()} or with the redefinition of {@link Scheduler#stopCondition()}.
     */
    void waitUntilFinish();
}
