package mas.environment;

/**
 * A 2 dimensions continuous grid representing the environment of a MAS.
 */
public class TwoDCContinuousGrid {

    private final int width;
    private final int height;

    /**
     * Constructor of TwoDContinuousGrid.
     *
     * @param _width
     *          width of the environment
     * @param _height
     *          height of the environment
     */
    public TwoDCContinuousGrid(int _width, int _height){
        height = _height;
        width = _width;
    }

    /**
     * Getter of height.
     * @return the height of the grid
     */
    public int getHeight() {
        return height;
    }

    /**
     * Getter of width.
     * @return the width of the grid
     */
    public int getWidth() {
        return width;
    }

}
